import './Viewport.css';
import Homepage from '../Pages/Homepage/Homepage';
import Knowledge from '../Pages/Knowledge/Knowledge';
import Portfolio from '../Pages/Portfolio/Portfolio';
import Contact from '../Pages/Contact/Contact';

function Viewport() {
  return (
    <div className='Viewport'>
        <Homepage/>
        <Knowledge/>
        <Portfolio/>
        <Contact/>
    </div>
  );
}

export default Viewport;
