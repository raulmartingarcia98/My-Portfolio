import "./Header.css";

function Header() {
  const paginate = (page) => {
    document.getElementsByClassName("Viewport")[0].style.marginLeft =
      page * -100 + "vw";

    switch (page) {
      case 0:
      default:
        document.getElementsByTagName("li")[0].style.color =
          "var(--clr-gradient-4)";
        document.getElementsByTagName("li")[1].style.color =
          "var(--clr-gradient-4)";
        document.getElementsByTagName("li")[2].style.color =
          "var(--clr-gradient-4)";
        document.getElementsByTagName("li")[3].style.color =
          "var(--clr-gradient-4)";
        break;
      case 1:
        document.getElementsByTagName("li")[0].style.color =
          "var(--clr-gradient-4)";
        document.getElementsByTagName("li")[1].style.color =
          "var(--clr-gradient-4)";
        document.getElementsByTagName("li")[2].style.color =
          "var(--clr-gradient-4)";
        document.getElementsByTagName("li")[3].style.color =
          "var(--clr-gradient-4)";
        break;
      case 2:
        document.getElementsByTagName("li")[0].style.color =
          "var(--clr-gradient-1)";
        document.getElementsByTagName("li")[1].style.color =
          "var(--clr-gradient-1)";
        document.getElementsByTagName("li")[2].style.color =
          "var(--clr-gradient-1)";
        document.getElementsByTagName("li")[3].style.color =
          "var(--clr-gradient-1)";
        break;
      case 3:
        document.getElementsByTagName("li")[0].style.color =
          "var(--clr-gradient-1)";
        document.getElementsByTagName("li")[1].style.color =
          "var(--clr-gradient-1)";
        document.getElementsByTagName("li")[2].style.color =
          "var(--clr-gradient-1)";
        document.getElementsByTagName("li")[3].style.color =
          "var(--clr-gradient-1)";
        break;
    }
  };

  return (
    <header className="Header">
      <nav>
        <ul>
          <li className=" clr-black" onClick={() => paginate(0)}>
            Home
          </li>
          <li className=" clr-black" onClick={() => paginate(1)}>
            Knowledge
          </li>
          <li className=" clr-black" onClick={() => paginate(2)}>
            Portfolio
          </li>
          <li className=" clr-black" onClick={() => paginate(3)}>
            Contact
          </li>
        </ul>
      </nav>
    </header>
  );
}

export default Header;
