import "./ArrowButton.css";

function ArrowButton() {
const paginate = (page) => {
    document.getElementsByClassName("Viewport")[0].style.marginLeft =
      page * -100 + "vw";

    switch (page) {
      case 0:
      default:
        document.getElementsByTagName("li")[0].style.color =
          "var(--clr-gradient-4)";
        document.getElementsByTagName("li")[1].style.color =
          "var(--clr-gradient-4)";
        document.getElementsByTagName("li")[2].style.color =
          "var(--clr-gradient-4)";
        document.getElementsByTagName("li")[3].style.color =
          "var(--clr-gradient-4)";
        break;
      case 1:
        document.getElementsByTagName("li")[0].style.color =
          "var(--clr-gradient-4)";
        document.getElementsByTagName("li")[1].style.color =
          "var(--clr-gradient-4)";
        document.getElementsByTagName("li")[2].style.color =
          "var(--clr-gradient-4)";
        document.getElementsByTagName("li")[3].style.color =
          "var(--clr-gradient-4)";
        break;
      case 2:
        document.getElementsByTagName("li")[0].style.color =
          "var(--clr-gradient-1)";
        document.getElementsByTagName("li")[1].style.color =
          "var(--clr-gradient-1)";
        document.getElementsByTagName("li")[2].style.color =
          "var(--clr-gradient-1)";
        document.getElementsByTagName("li")[3].style.color =
          "var(--clr-gradient-1)";
        break;
      case 3:
        document.getElementsByTagName("li")[0].style.color =
          "var(--clr-gradient-1)";
        document.getElementsByTagName("li")[1].style.color =
          "var(--clr-gradient-1)";
        document.getElementsByTagName("li")[2].style.color =
          "var(--clr-gradient-1)";
        document.getElementsByTagName("li")[3].style.color =
          "var(--clr-gradient-1)";
        break;
    }
  };

  return (
    <div class="center-con">
    <div class="round"  onClick={() => paginate(1)}>
        <div id="cta">
            <span class="arrow primera next "></span>
            <span class="arrow segunda next "></span>
        </div>
    </div>
</div>
  );
}

export default ArrowButton;