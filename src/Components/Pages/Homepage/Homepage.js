import "./Homepage.css";
import ArrowButton from '../../Assets/ArrowButton/ArrowButton';

function Homepage() {
  return (
    <div className="Homepage page text-center">
      <div className="first-container">
        <h1 className="clr-black">Who i am?</h1>
        <h2>Raúl Martín García</h2>
        <span>Full-Stack Web Developer</span>
        <ArrowButton/>
      </div>
    </div>
  );
}

export default Homepage;
