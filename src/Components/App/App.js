import './App.css';
import Header from '../Header/Header';
import Viewport from '../Viewport/Viewport';

function App() {
  return (
    <div className="App">
      <Header />
      <Viewport />
    </div>
  );
}

export default App;
